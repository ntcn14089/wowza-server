#/bin/bash
wowza_version=4-7-0
wowza_version1=4.7.0
yum install -y lsb
distro=$(lsb_release -si)
echo $distro
case "$distro" in
 "Ubuntu" | "Debian" | "LinuxMint" ) sudo apt-get -y update && sudo apt-get -y install expect curl;;
 "Fedora" ) sudo dnf -y install expect curl;;
 "CentOS" | "RedHatEnterpriseServer" ) sudo yum -y upgrade && sudo yum -y install expect curl bind-utils java-openjdk;;
esac
echo "##### FINISHED CHECK SOME PACKAGE NEED, DONWLOADING WOWZA#####"
curl -o /tmp/wowza.run https://www.wowza.com/downloads/WowzaStreamingEngine-${wowza_version}/WowzaStreamingEngine-${wowza_version1}-linux-x64-installer.run
chmod 755 /tmp/wowza.run
./wowza.exp
echo "###########FINISH INSTALLED WOWZA###########"
service WowzaStreamingEngine start
service WowzaStreamingEngineManager start
echo "############WOWZA SERVER INFOMATION#########"
ip=$(dig TXT +short o-o.myaddr.l.google.com @ns1.google.com | awk -F'"' '{ print $2}')
curl -I http://$ip:1935/
echo "Please go to: http://$ip:8088/enginemanager "
echo "###########################################"
